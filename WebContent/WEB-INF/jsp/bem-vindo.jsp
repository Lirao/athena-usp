<%@page import="br.com.athena.objetos.Usuario"%>
<%@page import="br.com.athena.dao.UsuarioDao"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<html>

<body>

	<c:import url="/WEB-INF/jsp/cabecalho.jsp" />
	<div class="container">
		<div class="coment-form">
			<h3>Seja bem vindo ${papel} ${login}!</h3>
			<br>
				<h2>Perfil</h2>
			<br>
			<%
				int nUsp = (int) session.getAttribute("nUsp");
				UsuarioDao userDao = new UsuarioDao();
				Usuario user = new Usuario();
				user.setnUsp(nUsp);
				user = userDao.pesquisarUsuario(user);
			%>
			<form name="formUsuario">
					<p>N�mero USP:</p>
					<input type="text" id="nUsp" name="nUsp"required="true" value="<%=user.getnUsp()%>"/>
					<p>Nome:</p>
					<input type="text" id="nome" name="nome" required="true" value="<%=user.getNome()%>"/>
					<p>Sexo:</p>
					<input type="text" id="sexo" name="sexo" required="true" value="<%=user.getSexo()%>"/>
					<p>e-mail:</p>
					<input type="text" id="email" name="email" required="true" value="<%=user.getE_mail()%>"/>
					<p>Data de Nascimento (dd/mm/yyyy):</p>
					<input type="text" id="dtNasc" name="dtNasc" required="false" value="<%=user.getDt_nasc()%>"/>
					<p>Telefone:</p>
					<input type="text" id="telefone" name="telefone" required="true" value="<%=user.getTelefone()%>"/>
					<p>Celular:</p>
					<input type="text" id="celular" name="celular" required="true" value="<%=user.getCelular()%>"/>
					<p>Link curr�culo Lattes:</p>
					<input type="text" id="curriculoLattes"  name="curriculoLattes" required="false" value="<%=user.getCurriculo_lattes()%>"/>
			</form>
		</div>
	</div>
	<c:import url="/WEB-INF/jsp/rodape.jsp" />

</body>

</html>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.List"%>
<%@page import="br.com.athena.objetos.IniciacaoCientifica"%>
<%@page import="br.com.athena.objetos.Usuario"%>
<%@page import="br.com.athena.objetos.Aluno"%>
<%@page import="br.com.athena.objetos.Professor"%>
<%@page import="br.com.athena.dao.IniciacaoDao"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<html>
<head>
<link href="css/bootstrap.css" rel='stylesheet' type='text/css' />
<!-- jQuery (necessary JavaScript plugins) -->
<script src="js/bootstrap.js"></script>
<!-- Custom Theme files -->
<link href="css/style.css" rel='stylesheet' type='text/css' />
<!-- Custom Theme files -->
<!--//theme-style-->
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords"
	content="Flooring  Responsive web template, Bootstrap Web Templates, Flat Web Templates, Andriod Compatible web template, 
Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyErricsson, Motorola web design" />
<script type="application/x-javascript">

	 addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } 

</script>

<script src="js/jquery.min.js"></script>
<!--/animated-css-->
<script type="text/javascript" src="js/move-top.js"></script>
<script type="text/javascript" src="js/easing.js"></script>
<!--/script-->
<script type="text/javascript">
	jQuery(document).ready(function($) {
		$(".scroll").click(function(event) {
			event.preventDefault();
			$('html,body').animate({
				scrollTop : $(this.hash).offset().top
			}, 900);
		});
	});
</script>
<!--script-->
</head>
<body>

	<c:import url="/WEB-INF/jsp/cabecalho.jsp" />
	<div class="single">
		<div class="container">
			<!-- projects -->
			<div class="coment-form">
				<h3 class="prjt">Cadastro de Inicia��o Cientifica</h3>
				
					<font color="red"><p>${erro}</p></font>
					<br>
					
				<form action="mvc?do=InscreverICController&codIC=${codIC}" method="POST">
					<p>Nome da Inicia��o:</p>
					<input type="text" name="nomeIC" required="true" disabled="true" value="${nomeIC}">
					<p>Professor Orientador:</p>
					<input type="text" name="nomeProf" required="true" disabled="true" value="${nomeProf}">
					<p>Status:</p>
					<input type="text" name="statusIC" required="true" disabled="true" value="${statusIC}">
					<p>Descri��o:</p>
					<textarea id="descricao" name="descricao" rows="5" cols="100" required="true" disabled="true">${descIC}</textarea>
					<p>Dura��o:</p>
					<input type="text" name="duracao" required="true" disabled="true" value="${duracaoIC}">
					<p>Cursos:</p>
					<input type="text" name="cursos" required="true" disabled="true" value="${cursosIC}">
					<br>
				<%
					try{
					String papel = (String) session.getAttribute("papel");
					Boolean ehAluno = (Boolean) session.getAttribute("ehAluno");
					int nUsp = (int) session.getAttribute("nUsp");
					if (papel != null && !ehAluno) {
						int codIC = (int) request.getAttribute("codIC");
						IniciacaoDao icDao = new IniciacaoDao();
						IniciacaoCientifica ic = icDao.listarICPorId(Integer.toString(codIC));
						if(nUsp == ic.getProfessor().getnUsp()){
							%>
							<a href="mvc?do=AlterarICController&codIC=<%=ic.getCodIC()%>">Editar</a>   |   
							<% if(ic.getStatus().equals("Finalizada")){ %>
								<a href="mvc?do=AbrirICController&codIC=<%=ic.getCodIC()%>">Reabrir processo</a>   |
							<%}
							else{%>
								<a href="mvc?do=FinalizarICController&codIC=<%=ic.getCodIC()%>">Finalizar processo</a>   |   
							<%
								}
							%>
							<a href="mvc?do=RemoverICController&codIC=<%=ic.getCodIC()%>">Excluir</a>
							<%	
						}
				%>
					<h1>Alunos inscritos:</h1>
					<table class="table">
					<tr>
						<td>Nome do aluno</td>
						<td>nUsp</td>
						<td>E-mail</td>
						<td>Previs�o forma��o</td>
					</tr>
					<%
						if(ic.getAluno() != null){
						for (Aluno alu : ic.getAluno()) {
					%>
					<tr>
						<td><%=alu.getNome()%></td>
						<td><%=alu.getnUsp()%></td>
						<td><%=alu.getE_mail()%></td>
						<td><%=alu.getAnoPrev_formacao()%></td>
					</tr>
					<%
							}
						}
					%>
					</table>
				<% 
					}
					else if(papel!=null){
				%>
						<input type="submit" value="Inscrever-se">
					<%
						}
					else{
					%>
						<a href="login.jsp">Fa�a login</a>
					<% 
						}
					}
				catch(Exception ex){
					request.setAttribute("erro", "Erro no processamento da pagina!");
				}
					%>
				</form>
			</div>
		</div>
	</div>

	<c:import url="/WEB-INF/jsp/rodape.jsp" />
</body>

</html>
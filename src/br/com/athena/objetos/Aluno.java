package br.com.athena.objetos;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;
import javax.persistence.JoinColumn;
import javax.persistence.UniqueConstraint;

@Entity
@Table
@PrimaryKeyJoinColumn(name = "nUsp")
public class Aluno extends Usuario {
	private String curso;
	private Date anoPrev_formacao;
	private String intereses_pequisa;
	@ManyToMany(fetch=FetchType.EAGER)
	@JoinTable(name = "aluno_iniciacao", joinColumns = @JoinColumn(name = "nUsp") , inverseJoinColumns = @JoinColumn(name = "codIC"), 
            uniqueConstraints = @UniqueConstraint(columnNames = {"nUsp", "codIC"}))
	private List<IniciacaoCientifica> iniciacao;

	public Aluno() {
		this.iniciacao = new ArrayList<IniciacaoCientifica>();
	}

	public Aluno(int nUsp, String senha) {
		this.iniciacao = new ArrayList<IniciacaoCientifica>();
		this.setnUsp(nUsp);
		this.setSenha(senha);
	}

	public List<IniciacaoCientifica> getListaIC() {
		return iniciacao;
	}

	public void setListaIC(List<IniciacaoCientifica> listaIC) {
		this.iniciacao = listaIC;
	}

	public String getCurso() {
		return curso;
	}

	public void setCurso(String curso) {
		this.curso = curso;
	}

	public Date getAnoPrev_formacao() {
		return anoPrev_formacao;
	}

	public void setAnoPrev_formacao(Date anoPrev_formacao) {
		this.anoPrev_formacao = anoPrev_formacao;
	}

	public String getIntereses_pequisa() {
		return intereses_pequisa;
	}

	public void setIntereses_pequisa(String intereses_pequisa) {
		this.intereses_pequisa = intereses_pequisa;
	}
}

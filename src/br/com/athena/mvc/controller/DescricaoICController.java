package br.com.athena.mvc.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import br.com.athena.dao.IniciacaoDao;
import br.com.athena.mvc.controller.servlet.ServletImpl;
import br.com.athena.objetos.IniciacaoCientifica;

public class DescricaoICController implements ServletImpl {

	public String executa(HttpServletRequest req, HttpServletResponse res) {
		try{
		String paramBusca = req.getParameter("codIC");
		if(paramBusca==null)
			return "buscaIC.jsp";
		IniciacaoDao icDao = new IniciacaoDao();
		IniciacaoCientifica ic = icDao.listarICPorId(paramBusca);
		req.setAttribute("codIC", ic.getCodIC());
		req.setAttribute("cursosIC", ic.getCursos());
		req.setAttribute("descIC", ic.getDescricao());
		req.setAttribute("duracaoIC", ic.getDuracao());
		req.setAttribute("nomeIC", ic.getNomeIC());
		req.setAttribute("statusIC", ic.getStatus());
		req.setAttribute("nomeProf", ic.getProfessor().getNome());
		return "descricaoIC.jsp";
		}
		catch(Exception ex){		
			return "buscaIC.jsp";
		}
	}
}
package br.com.athena.mvc.controller;

import java.text.ParseException;
import java.text.SimpleDateFormat;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import br.com.athena.dao.ProfessorDao;
import br.com.athena.mvc.controller.servlet.ServletImpl;
import br.com.athena.objetos.Professor;

public class AdicionaProfessorController implements ServletImpl {

	public String executa(HttpServletRequest req, HttpServletResponse res) {
		Professor prof = new Professor();
		ProfessorDao profDao = new ProfessorDao();
		try {
			// informa��es de usuario
			prof.setPapel(req.getParameter("papel"));
			prof.setnUsp(Integer.parseInt(req.getParameter("nUsp")));
			prof.setNome(req.getParameter("nome"));
			prof.setSexo(req.getParameter("sexo"));
			prof.setE_mail(req.getParameter("email"));
			prof.setDt_nasc(new SimpleDateFormat("dd/MM/yyyy").parse(req.getParameter("dtNasc")));
			prof.setTelefone(req.getParameter("telefone"));
			prof.setCelular(req.getParameter("celular"));
			prof.setCurriculo_lattes(req.getParameter("curriculo"));
			prof.setSenha(req.getParameter("senha"));
			// informa��es do professor
			prof.setCurso_ministra(req.getParameter("cursoMinistra"));
			prof.setLinha_pesquisa(req.getParameter("linhaPesquisa"));

			if (profDao.adicionarProfessor(prof, prof) != null) {
				req.setAttribute("erro", "Cadastro realizado com sucesso!");
			} else {
				req.setAttribute("erro", "Usu�rio j� existe!");
			}
			return "login.jsp";
		}
		catch(ParseException ex){
			req.setAttribute("erro", "Data informada incorretamente!");
			return "cadastroUsuario.jsp";
		}
		catch(NumberFormatException ex){
			req.setAttribute("erro", "N�mero Usp inv�lido!");
			return "cadastroUsuario.jsp";
		}
		catch (javax.persistence.RollbackException ex) {
			try {
				profDao.adicionarProfessor( prof, prof);
				req.setAttribute("erro", "Cadastro realizado com sucesso!");
				return "login.jsp";
			} catch (Exception e) {
				req.setAttribute("erro", "Usu�rio j� cadastrado!");
				System.out.println(e);
				return "cadastroUsuario.jsp";
			}
		}
	}
}
package br.com.athena.mvc.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import br.com.athena.dao.IniciacaoDao;
import br.com.athena.mvc.controller.servlet.ServletImpl;
import br.com.athena.objetos.IniciacaoCientifica;

public class FinalizarICController implements ServletImpl {
	public String executa(HttpServletRequest req, HttpServletResponse res) {
		String codIC = (String) req.getParameter("codIC");
		try{
			
			IniciacaoDao icDao = new IniciacaoDao();
			IniciacaoCientifica ic = icDao.listarICPorId(codIC);
			
			ic.setStatus("Finalizada");
			icDao.atualizaIC(ic);
			req.setAttribute("erro", "IC finalizada com sucesso!");
			
			return "mvc?do=DescricaoICController&codIC="+codIC;
		}
		catch(Exception ex){
			req.setAttribute("erro", "Erro na finalização da IC!");
			return "mvc?do=DescricaoICController&codIC="+codIC;
		}
	}
}

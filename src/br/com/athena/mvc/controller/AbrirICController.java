package br.com.athena.mvc.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import br.com.athena.dao.IniciacaoDao;
import br.com.athena.mvc.controller.servlet.ServletImpl;
import br.com.athena.objetos.IniciacaoCientifica;

public class AbrirICController implements ServletImpl {
	public String executa(HttpServletRequest req, HttpServletResponse res) {
		String codIC = (String) req.getParameter("codIC");
		try{
			
			IniciacaoDao icDao = new IniciacaoDao();
			IniciacaoCientifica ic = icDao.listarICPorId(codIC);
			
			ic.setStatus("Aberta");
			icDao.atualizaIC(ic);
			req.setAttribute("erro", "IC reaberta com sucesso!");
			
			return "mvc?do=DescricaoICController&codIC="+codIC;
		}
		catch(Exception ex){
			req.setAttribute("erro", "Erro na abertura da IC!");
			return "mvc?do=DescricaoICController&codIC="+codIC;
		}
	}
}

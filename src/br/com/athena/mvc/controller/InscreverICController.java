package br.com.athena.mvc.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import br.com.athena.dao.AlunoDao;
import br.com.athena.dao.IniciacaoDao;
import br.com.athena.mvc.controller.servlet.ServletImpl;
import br.com.athena.objetos.Aluno;
import br.com.athena.objetos.IniciacaoCientifica;

public class InscreverICController implements ServletImpl {

	public String executa(HttpServletRequest req, HttpServletResponse res) {
		IniciacaoCientifica iniciacaoCientifica = new IniciacaoCientifica();
		try {			
			String codIC = (String) req.getParameter("codIC");
			int nUsp = (int) req.getSession().getAttribute("nUsp");
			String senha = (String) req.getSession().getAttribute("senha");

			IniciacaoDao iniciacaoDao = new IniciacaoDao();
			iniciacaoCientifica = iniciacaoDao.listarICPorId(codIC);
			if(iniciacaoCientifica.getStatus().equals("Finalizada")){
				req.setAttribute("erro", "Fase de inscri��o j� conclu�da!");
				return "mvc?do=DescricaoICController&codIC=" + iniciacaoCientifica.getCodIC();
			}
			
			AlunoDao alunoDao = new AlunoDao();
			Aluno aluno = new Aluno(nUsp, senha);
			aluno = alunoDao.pesquisarAluno(aluno);
			aluno.getListaIC().add(iniciacaoCientifica);
			alunoDao.atualizaAluno(aluno);
			req.getSession().setAttribute("aluno", aluno);
			req.setAttribute("codIC", iniciacaoCientifica.getCodIC());
			req.setAttribute("erro", "Inscri��o realizada com sucesso!");
			return "mvc?do=DescricaoICController&codIC=" + iniciacaoCientifica.getCodIC();
		} catch (Exception e) {
			e.printStackTrace();
			req.setAttribute("erro", "Voc� j� est� inscrito!");
		}
		return "mvc?do=DescricaoICController&codIC=" + iniciacaoCientifica.getCodIC();
	}
}
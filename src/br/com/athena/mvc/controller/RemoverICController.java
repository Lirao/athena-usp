package br.com.athena.mvc.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import br.com.athena.dao.IniciacaoDao;
import br.com.athena.mvc.controller.servlet.ServletImpl;
import br.com.athena.objetos.IniciacaoCientifica;

public class RemoverICController implements ServletImpl {

	public String executa(HttpServletRequest req, HttpServletResponse res) {
		String codIC = (String) req.getParameter("codIC");
		try {

			IniciacaoDao iniciacaoDao = new IniciacaoDao();
			IniciacaoCientifica iniciacaoCientifica = iniciacaoDao.listarICPorId(codIC);

			iniciacaoDao.removerIC(iniciacaoCientifica);
			req.setAttribute("erro", "IC removida com sucesso!");
			return "buscaIC.jsp";

		} catch (Exception e) {
			req.setAttribute("erro", "N�o foi poss�vel remover a IC, existem alunos inscritos! <br>Ao inv�z disso, finalize o processo de inscri��o para arquivar a IC.");
			return "mvc?do=DescricaoICController&codIC="+codIC;
		}
	}
}
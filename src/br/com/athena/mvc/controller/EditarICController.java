package br.com.athena.mvc.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import br.com.athena.dao.IniciacaoDao;
import br.com.athena.mvc.controller.servlet.ServletImpl;
import br.com.athena.objetos.IniciacaoCientifica;

public class EditarICController implements ServletImpl {

	public String executa(HttpServletRequest req, HttpServletResponse res) {
		String codIC = (String) req.getParameter("codIC");
		try {
			IniciacaoDao IcDao = new IniciacaoDao();
			IniciacaoCientifica ic = IcDao.listarICPorId(codIC);

			// informações da iniciacao cientifica
			ic.setCursos(req.getParameter("cursos"));
			ic.setDescricao(req.getParameter("descricao"));
			ic.setDuracao(req.getParameter("duracao"));
			ic.setNomeIC(req.getParameter("nomeIC"));

			IcDao.atualizaIC(ic);
			
			req.setAttribute("erro", "IC atualizada com sucesso!");
		} catch (Exception e) {
			req.setAttribute("erro", "Ocorreu um problema na alteração da IC!"+codIC+" <-");
			System.out.println(e);
		}
		return "mvc?do=DescricaoICController&codIC="+codIC;
	}
}
